<?php

namespace Tests\Feature\Api\Endpoints;

use App\Models\User;
use App\Models\Transaction;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TransactionsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Currently logged in User.
     *
     * @var User
     */
    protected $user;

    /**
     * Current endpoint url which being tested.
     *
     * @var string
     */
    protected $endpoint = '/api/transactions/';

    /**
     * Faker generator instance.
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * The model which being tested.
     *
     * @var Transaction
     */
    protected $model;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->faker = new Generator();
        $this->user = User::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->user, config('api.cms_guard'));

        $this->model = Transaction::factory()->create();
    }

    /** @test */
    public function index_endpoint_works_as_expected()
    {
        $this->getJson($this->endpoint)
            ->assertStatus(200)
            ->assertJsonFragment([
                'book_id' => $this->model->getAttribute('book_id'),
                'user_id' => $this->model->getAttribute('user_id'),
                'date' => $this->model->getAttribute('date'),
                'quantity' => $this->model->getAttribute('quantity'),
                'total' => $this->model->getAttribute('total'),
            ]);
    }

    /** @test */
    public function show_endpoint_works_as_expected()
    {
        $this->getJson($this->endpoint.$this->model->getKey())
            ->assertStatus(200)
            ->assertJsonFragment([
                'book_id' => $this->model->getAttribute('book_id'),
                'user_id' => $this->model->getAttribute('user_id'),
                'date' => $this->model->getAttribute('date'),
                'quantity' => $this->model->getAttribute('quantity'),
                'total' => $this->model->getAttribute('total'),
            ]);
    }

    /** @test */
    public function create_endpoint_works_as_expected()
    {
        // Submitted data
        $data = Transaction::factory()->raw();

        // The data which should be shown
        $seenData = $data;

        $this->postJson($this->endpoint, $data)
            ->assertStatus(201)
            ->assertJsonFragment($seenData);
    }

    /** @test */
    public function update_endpoint_works_as_expected()
    {
        // Submitted data
        $data = Transaction::factory()->raw();

        // The data which should be shown
        $seenData = $data;

        $this->patchJson($this->endpoint.$this->model->getKey(), $data)
            ->assertStatus(200)
            ->assertJsonFragment($seenData);
    }

    /** @test */
    public function delete_endpoint_works_as_expected()
    {
        $this->deleteJson($this->endpoint.$this->model->getKey())
            ->assertStatus(200)
            ->assertJsonFragment([
                'info' => 'The transaction has been deleted.',
            ]);

        $this->assertDatabaseHas('transactions', [
            'book_id' => $this->model->getAttribute('book_id'),
            'user_id' => $this->model->getAttribute('user_id'),
            'date' => $this->model->getAttribute('date'),
            'quantity' => $this->model->getAttribute('quantity'),
            'total' => $this->model->getAttribute('total'),
        ]);

        $this->assertDatabaseMissing('transactions', [
            'book_id' => $this->model->getAttribute('book_id'),
            'user_id' => $this->model->getAttribute('user_id'),
            'date' => $this->model->getAttribute('date'),
            'quantity' => $this->model->getAttribute('quantity'),
            'total' => $this->model->getAttribute('total'),
            'deleted_at' => null,
        ]);
    }
}

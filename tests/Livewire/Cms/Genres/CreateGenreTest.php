<?php

namespace Tests\Livewire\Cms\Genres;

use App\Models\Admin;
use App\Models\Genre;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class CreateGenreTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));
    }

    /** @test */
    public function create_component_is_accessible()
    {
        Livewire::test('cms.genres.create-genre')
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_save_the_new_genre_record()
    {
        $data = $this->fakeRawData(Genre::class);

        Livewire::test('cms.genres.create-genre')
            ->set('genre.name', $data['name'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/genres');

        $this->assertDatabaseHas('genres', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The new genre has been saved.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_creating_new_genre_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Genre::class);

        Livewire::test('cms.genres.create-genre')
            ->set('genre.name', $data['name'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/genres');

        $this->assertDatabaseMissing('genres', $data);
    }
}

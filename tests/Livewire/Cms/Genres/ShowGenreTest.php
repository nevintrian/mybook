<?php

namespace App\Http\Livewire\Cms\Genres;

use App\Models\Admin;
use App\Models\Genre;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class ShowGenreTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Genre instance to support any test cases.
     *
     * @var Genre
     */
    protected Genre $genre;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->genre = Genre::factory()->create();
    }

    /** @test */
    public function show_component_is_accessible()
    {
        Livewire::test('cms.genres.show-genre', ['genre' => $this->genre])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_guide_admin_to_the_edit_genre_page()
    {
        Livewire::test('cms.genres.show-genre', ['genre' => $this->genre])
            ->call('edit')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/genres/'. $this->genre->getKey() .'/edit');
    }

    /** @test */
    public function it_can_go_back_to_index_page()
    {
        Livewire::test('cms.genres.show-genre', ['genre' => $this->genre])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/genres');
    }
}

<?php

namespace Tests\Livewire\Cms\Groups;

use App\Models\Admin;
use App\Models\Group;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class CreateGroupTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));
    }

    /** @test */
    public function create_component_is_accessible()
    {
        Livewire::test('cms.groups.create-group')
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_save_the_new_group_record()
    {
        $data = $this->fakeRawData(Group::class);

        Livewire::test('cms.groups.create-group')
            ->set('group.roles', $data['roles'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/groups');

        $this->assertDatabaseHas('groups', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The new group has been saved.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_creating_new_group_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Group::class);

        Livewire::test('cms.groups.create-group')
            ->set('group.roles', $data['roles'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/groups');

        $this->assertDatabaseMissing('groups', $data);
    }
}

<?php

namespace Tests\Livewire\Cms\Groups;

use App\Models\Admin;
use App\Models\Group;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class EditGroupTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Group instance to support any test cases.
     *
     * @var Group
     */
    protected Group $group;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->group = Group::factory()->create();
    }

    /** @test */
    public function edit_component_is_accessible()
    {
        Livewire::test('cms.groups.edit-group', ['group' => $this->group])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_update_the_existing_group_record()
    {
        $data = $this->fakeRawData(Group::class);

        Livewire::test('cms.groups.edit-group', ['group' => $this->group])
            ->set('group.roles', $data['roles'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/groups');

        $this->assertDatabaseHas('groups', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The group has been updated.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_updating_existing_group_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Group::class);

        Livewire::test('cms.groups.edit-group', ['group' => $this->group])
            ->set('group.roles', $data['roles'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/groups');

        $this->assertDatabaseMissing('groups', $data);
    }
}

<?php

namespace App\Http\Livewire\Cms\Groups;

use App\Models\Admin;
use App\Models\Group;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class ShowGroupTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Group instance to support any test cases.
     *
     * @var Group
     */
    protected Group $group;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->group = Group::factory()->create();
    }

    /** @test */
    public function show_component_is_accessible()
    {
        Livewire::test('cms.groups.show-group', ['group' => $this->group])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_guide_admin_to_the_edit_group_page()
    {
        Livewire::test('cms.groups.show-group', ['group' => $this->group])
            ->call('edit')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/groups/'. $this->group->getKey() .'/edit');
    }

    /** @test */
    public function it_can_go_back_to_index_page()
    {
        Livewire::test('cms.groups.show-group', ['group' => $this->group])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/groups');
    }
}

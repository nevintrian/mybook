<?php

namespace Tests\Livewire\Cms\Members;

use App\Models\Admin;
use App\Models\Member;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class CreateMemberTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));
    }

    /** @test */
    public function create_component_is_accessible()
    {
        Livewire::test('cms.members.create-member')
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_save_the_new_member_record()
    {
        $data = $this->fakeRawData(Member::class);

        Livewire::test('cms.members.create-member')
            ->set('member.name', $data['name'])
            ->set('member.group_id', $data['group_id'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/members');

        $this->assertDatabaseHas('members', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The new member has been saved.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_creating_new_member_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Member::class);

        Livewire::test('cms.members.create-member')
            ->set('member.name', $data['name'])
            ->set('member.group_id', $data['group_id'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/members');

        $this->assertDatabaseMissing('members', $data);
    }
}

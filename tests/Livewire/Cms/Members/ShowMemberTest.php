<?php

namespace App\Http\Livewire\Cms\Members;

use App\Models\Admin;
use App\Models\Member;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class ShowMemberTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Member instance to support any test cases.
     *
     * @var Member
     */
    protected Member $member;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->member = Member::factory()->create();
    }

    /** @test */
    public function show_component_is_accessible()
    {
        Livewire::test('cms.members.show-member', ['member' => $this->member])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_guide_admin_to_the_edit_member_page()
    {
        Livewire::test('cms.members.show-member', ['member' => $this->member])
            ->call('edit')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/members/'. $this->member->getKey() .'/edit');
    }

    /** @test */
    public function it_can_go_back_to_index_page()
    {
        Livewire::test('cms.members.show-member', ['member' => $this->member])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/members');
    }
}

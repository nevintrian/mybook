<?php

namespace Tests\Livewire\Cms\Members;

use App\Models\Admin;
use App\Models\Member;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class EditMemberTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Member instance to support any test cases.
     *
     * @var Member
     */
    protected Member $member;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->member = Member::factory()->create();
    }

    /** @test */
    public function edit_component_is_accessible()
    {
        Livewire::test('cms.members.edit-member', ['member' => $this->member])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_update_the_existing_member_record()
    {
        $data = $this->fakeRawData(Member::class);

        Livewire::test('cms.members.edit-member', ['member' => $this->member])
            ->set('member.name', $data['name'])
            ->set('member.group_id', $data['group_id'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/members');

        $this->assertDatabaseHas('members', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The member has been updated.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_updating_existing_member_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Member::class);

        Livewire::test('cms.members.edit-member', ['member' => $this->member])
            ->set('member.name', $data['name'])
            ->set('member.group_id', $data['group_id'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/members');

        $this->assertDatabaseMissing('members', $data);
    }
}

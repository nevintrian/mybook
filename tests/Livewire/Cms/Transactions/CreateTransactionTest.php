<?php

namespace Tests\Livewire\Cms\Transactions;

use App\Models\Admin;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class CreateTransactionTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));
    }

    /** @test */
    public function create_component_is_accessible()
    {
        Livewire::test('cms.transactions.create-transaction')
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_save_the_new_transaction_record()
    {
        $data = $this->fakeRawData(Transaction::class);

        Livewire::test('cms.transactions.create-transaction')
            ->set('transaction.book_id', $data['book_id'])
            ->set('transaction.user_id', $data['user_id'])
            ->set('transaction.date', $data['date'])
            ->set('transaction.quantity', $data['quantity'])
            ->set('transaction.total', $data['total'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/transactions');

        $this->assertDatabaseHas('transactions', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The new transaction has been saved.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_creating_new_transaction_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Transaction::class);

        Livewire::test('cms.transactions.create-transaction')
            ->set('transaction.book_id', $data['book_id'])
            ->set('transaction.user_id', $data['user_id'])
            ->set('transaction.date', $data['date'])
            ->set('transaction.quantity', $data['quantity'])
            ->set('transaction.total', $data['total'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/transactions');

        $this->assertDatabaseMissing('transactions', $data);
    }
}

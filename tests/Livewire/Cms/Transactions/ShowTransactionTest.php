<?php

namespace App\Http\Livewire\Cms\Transactions;

use App\Models\Admin;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class ShowTransactionTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Transaction instance to support any test cases.
     *
     * @var Transaction
     */
    protected Transaction $transaction;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->transaction = Transaction::factory()->create();
    }

    /** @test */
    public function show_component_is_accessible()
    {
        Livewire::test('cms.transactions.show-transaction', ['transaction' => $this->transaction])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_guide_admin_to_the_edit_transaction_page()
    {
        Livewire::test('cms.transactions.show-transaction', ['transaction' => $this->transaction])
            ->call('edit')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/transactions/'. $this->transaction->getKey() .'/edit');
    }

    /** @test */
    public function it_can_go_back_to_index_page()
    {
        Livewire::test('cms.transactions.show-transaction', ['transaction' => $this->transaction])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/transactions');
    }
}

<?php

namespace Tests\Livewire\Cms\Transactions;

use App\Models\Admin;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class EditTransactionTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Transaction instance to support any test cases.
     *
     * @var Transaction
     */
    protected Transaction $transaction;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->transaction = Transaction::factory()->create();
    }

    /** @test */
    public function edit_component_is_accessible()
    {
        Livewire::test('cms.transactions.edit-transaction', ['transaction' => $this->transaction])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_update_the_existing_transaction_record()
    {
        $data = $this->fakeRawData(Transaction::class);

        Livewire::test('cms.transactions.edit-transaction', ['transaction' => $this->transaction])
            ->set('transaction.book_id', $data['book_id'])
            ->set('transaction.user_id', $data['user_id'])
            ->set('transaction.date', $data['date'])
            ->set('transaction.quantity', $data['quantity'])
            ->set('transaction.total', $data['total'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/transactions');

        $this->assertDatabaseHas('transactions', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The transaction has been updated.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_updating_existing_transaction_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Transaction::class);

        Livewire::test('cms.transactions.edit-transaction', ['transaction' => $this->transaction])
            ->set('transaction.book_id', $data['book_id'])
            ->set('transaction.user_id', $data['user_id'])
            ->set('transaction.date', $data['date'])
            ->set('transaction.quantity', $data['quantity'])
            ->set('transaction.total', $data['total'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/transactions');

        $this->assertDatabaseMissing('transactions', $data);
    }
}

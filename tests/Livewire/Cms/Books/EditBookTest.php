<?php

namespace Tests\Livewire\Cms\Books;

use App\Models\Admin;
use App\Models\Book;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class EditBookTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Book instance to support any test cases.
     *
     * @var Book
     */
    protected Book $book;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->book = Book::factory()->create();
    }

    /** @test */
    public function edit_component_is_accessible()
    {
        Livewire::test('cms.books.edit-book', ['book' => $this->book])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_update_the_existing_book_record()
    {
        $data = $this->fakeRawData(Book::class);

        Livewire::test('cms.books.edit-book', ['book' => $this->book])
            ->set('book.author_id', $data['author_id'])
            ->set('book.title', $data['title'])
            ->set('book.price', $data['price'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/books');

        $this->assertDatabaseHas('books', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The book has been updated.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_updating_existing_book_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Book::class);

        Livewire::test('cms.books.edit-book', ['book' => $this->book])
            ->set('book.author_id', $data['author_id'])
            ->set('book.title', $data['title'])
            ->set('book.price', $data['price'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/books');

        $this->assertDatabaseMissing('books', $data);
    }
}

<?php

namespace App\Http\Livewire\Cms\Books;

use App\Models\Admin;
use App\Models\Book;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class ShowBookTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Book instance to support any test cases.
     *
     * @var Book
     */
    protected Book $book;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->book = Book::factory()->create();
    }

    /** @test */
    public function show_component_is_accessible()
    {
        Livewire::test('cms.books.show-book', ['book' => $this->book])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_guide_admin_to_the_edit_book_page()
    {
        Livewire::test('cms.books.show-book', ['book' => $this->book])
            ->call('edit')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/books/'. $this->book->getKey() .'/edit');
    }

    /** @test */
    public function it_can_go_back_to_index_page()
    {
        Livewire::test('cms.books.show-book', ['book' => $this->book])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/books');
    }
}

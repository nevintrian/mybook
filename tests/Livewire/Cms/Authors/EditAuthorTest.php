<?php

namespace Tests\Livewire\Cms\Authors;

use App\Models\Admin;
use App\Models\Author;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class EditAuthorTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Author instance to support any test cases.
     *
     * @var Author
     */
    protected Author $author;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->author = Author::factory()->create();
    }

    /** @test */
    public function edit_component_is_accessible()
    {
        Livewire::test('cms.authors.edit-author', ['author' => $this->author])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_update_the_existing_author_record()
    {
        $data = $this->fakeRawData(Author::class);

        Livewire::test('cms.authors.edit-author', ['author' => $this->author])
            ->set('author.name', $data['name'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/authors');

        $this->assertDatabaseHas('authors', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The author has been updated.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_updating_existing_author_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Author::class);

        Livewire::test('cms.authors.edit-author', ['author' => $this->author])
            ->set('author.name', $data['name'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/authors');

        $this->assertDatabaseMissing('authors', $data);
    }
}

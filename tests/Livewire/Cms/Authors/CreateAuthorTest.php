<?php

namespace Tests\Livewire\Cms\Authors;

use App\Models\Admin;
use App\Models\Author;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class CreateAuthorTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));
    }

    /** @test */
    public function create_component_is_accessible()
    {
        Livewire::test('cms.authors.create-author')
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_save_the_new_author_record()
    {
        $data = $this->fakeRawData(Author::class);

        Livewire::test('cms.authors.create-author')
            ->set('author.name', $data['name'])
            ->call('save')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/authors');

        $this->assertDatabaseHas('authors', $data);

        self::assertEquals('success', session('alertType'));
        self::assertEquals('The new author has been saved.', session('alertMessage'));
    }

    /** @test */
    public function it_can_cancel_creating_new_author_and_go_back_to_index_page()
    {
        $data = $this->fakeRawData(Author::class);

        Livewire::test('cms.authors.create-author')
            ->set('author.name', $data['name'])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/authors');

        $this->assertDatabaseMissing('authors', $data);
    }
}

<?php

namespace App\Http\Livewire\Cms\Authors;

use App\Models\Admin;
use App\Models\Author;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\CmsTests;
use Tests\TestCase;

class ShowAuthorTest extends TestCase
{
    use CmsTests;
    use DatabaseMigrations;

    /**
     * Cms Admin Object.
     *
     * @var \App\Models\Admin
     */
    protected Admin $admin;

    /**
     * The Author instance to support any test cases.
     *
     * @var Author
     */
    protected Author $author;

    /**
     * Setup the test environment.
     *
     * return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(['PermissionSeeder', 'RoleSeeder']);

        $this->admin = Admin::factory()->create()->assignRole('super-administrator');

        $this->actingAs($this->admin, config('cms.guard'));

        $this->author = Author::factory()->create();
    }

    /** @test */
    public function show_component_is_accessible()
    {
        Livewire::test('cms.authors.show-author', ['author' => $this->author])
            ->assertHasNoErrors();
    }

    /** @test */
    public function it_can_guide_admin_to_the_edit_author_page()
    {
        Livewire::test('cms.authors.show-author', ['author' => $this->author])
            ->call('edit')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/authors/'. $this->author->getKey() .'/edit');
    }

    /** @test */
    public function it_can_go_back_to_index_page()
    {
        Livewire::test('cms.authors.show-author', ['author' => $this->author])
            ->call('backToIndex')
            ->assertHasNoErrors()
            ->assertRedirect('/cms/authors');
    }
}

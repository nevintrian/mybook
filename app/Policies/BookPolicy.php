<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Book;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookPolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the User can view any models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $this->can($user, new Book(), 'viewAny');
    }

    /**
     * Determine whether the User can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Book $book
     *
     * @return bool
     */
    public function view(User $user, Book $book): bool
    {
        return $this->can($user, $book, 'view');
    }

    /**
     * Determine whether the User can create models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->can($user, new Book(), 'create');
    }

    /**
     * Determine whether the User can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Book $book
     *
     * @return bool
     */
    public function update(User $user, Book $book): bool
    {
        return $this->can($user, $book, 'update');
    }

    /**
     * Determine whether the User can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Book $book
     *
     * @return bool
     */
    public function delete(User $user, Book $book): bool
    {
        return $this->can($user, $book, 'delete');
    }

    /**
     * Determine whether the User can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Book $book
     *
     * @return bool
     */
    public function restore(User $user, Book $book): bool
    {
        return $this->can($user, $book, 'restore');
    }

    /**
     * Determine whether the User can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Book $book
     *
     * @return bool
     */
    public function forceDelete(User $user, Book $book): bool
    {
        return $this->can($user, $book, 'forceDelete');
    }
}

<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Member;
use Illuminate\Auth\Access\HandlesAuthorization;

class MemberPolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the User can view any models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $this->can($user, new Member(), 'viewAny');
    }

    /**
     * Determine whether the User can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Member $member
     *
     * @return bool
     */
    public function view(User $user, Member $member): bool
    {
        return $this->can($user, $member, 'view');
    }

    /**
     * Determine whether the User can create models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->can($user, new Member(), 'create');
    }

    /**
     * Determine whether the User can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Member $member
     *
     * @return bool
     */
    public function update(User $user, Member $member): bool
    {
        return $this->can($user, $member, 'update');
    }

    /**
     * Determine whether the User can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Member $member
     *
     * @return bool
     */
    public function delete(User $user, Member $member): bool
    {
        return $this->can($user, $member, 'delete');
    }

    /**
     * Determine whether the User can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Member $member
     *
     * @return bool
     */
    public function restore(User $user, Member $member): bool
    {
        return $this->can($user, $member, 'restore');
    }

    /**
     * Determine whether the User can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Member $member
     *
     * @return bool
     */
    public function forceDelete(User $user, Member $member): bool
    {
        return $this->can($user, $member, 'forceDelete');
    }
}

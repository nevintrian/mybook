<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Transaction;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the User can view any models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $this->can($user, new Transaction(), 'viewAny');
    }

    /**
     * Determine whether the User can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Transaction $transaction
     *
     * @return bool
     */
    public function view(User $user, Transaction $transaction): bool
    {
        return $this->can($user, $transaction, 'view');
    }

    /**
     * Determine whether the User can create models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->can($user, new Transaction(), 'create');
    }

    /**
     * Determine whether the User can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Transaction $transaction
     *
     * @return bool
     */
    public function update(User $user, Transaction $transaction): bool
    {
        return $this->can($user, $transaction, 'update');
    }

    /**
     * Determine whether the User can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Transaction $transaction
     *
     * @return bool
     */
    public function delete(User $user, Transaction $transaction): bool
    {
        return $this->can($user, $transaction, 'delete');
    }

    /**
     * Determine whether the User can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Transaction $transaction
     *
     * @return bool
     */
    public function restore(User $user, Transaction $transaction): bool
    {
        return $this->can($user, $transaction, 'restore');
    }

    /**
     * Determine whether the User can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Transaction $transaction
     *
     * @return bool
     */
    public function forceDelete(User $user, Transaction $transaction): bool
    {
        return $this->can($user, $transaction, 'forceDelete');
    }
}

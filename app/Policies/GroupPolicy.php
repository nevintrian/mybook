<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Group;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the User can view any models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $this->can($user, new Group(), 'viewAny');
    }

    /**
     * Determine whether the User can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Group $group
     *
     * @return bool
     */
    public function view(User $user, Group $group): bool
    {
        return $this->can($user, $group, 'view');
    }

    /**
     * Determine whether the User can create models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->can($user, new Group(), 'create');
    }

    /**
     * Determine whether the User can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Group $group
     *
     * @return bool
     */
    public function update(User $user, Group $group): bool
    {
        return $this->can($user, $group, 'update');
    }

    /**
     * Determine whether the User can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Group $group
     *
     * @return bool
     */
    public function delete(User $user, Group $group): bool
    {
        return $this->can($user, $group, 'delete');
    }

    /**
     * Determine whether the User can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Group $group
     *
     * @return bool
     */
    public function restore(User $user, Group $group): bool
    {
        return $this->can($user, $group, 'restore');
    }

    /**
     * Determine whether the User can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Group $group
     *
     * @return bool
     */
    public function forceDelete(User $user, Group $group): bool
    {
        return $this->can($user, $group, 'forceDelete');
    }
}

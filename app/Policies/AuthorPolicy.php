<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Author;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuthorPolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the User can view any models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $this->can($user, new Author(), 'viewAny');
    }

    /**
     * Determine whether the User can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Author $author
     *
     * @return bool
     */
    public function view(User $user, Author $author): bool
    {
        return $this->can($user, $author, 'view');
    }

    /**
     * Determine whether the User can create models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->can($user, new Author(), 'create');
    }

    /**
     * Determine whether the User can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Author $author
     *
     * @return bool
     */
    public function update(User $user, Author $author): bool
    {
        return $this->can($user, $author, 'update');
    }

    /**
     * Determine whether the User can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Author $author
     *
     * @return bool
     */
    public function delete(User $user, Author $author): bool
    {
        return $this->can($user, $author, 'delete');
    }

    /**
     * Determine whether the User can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Author $author
     *
     * @return bool
     */
    public function restore(User $user, Author $author): bool
    {
        return $this->can($user, $author, 'restore');
    }

    /**
     * Determine whether the User can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Author $author
     *
     * @return bool
     */
    public function forceDelete(User $user, Author $author): bool
    {
        return $this->can($user, $author, 'forceDelete');
    }
}

<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Genre;
use Illuminate\Auth\Access\HandlesAuthorization;

class GenrePolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the User can view any models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $this->can($user, new Genre(), 'viewAny');
    }

    /**
     * Determine whether the User can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Genre $genre
     *
     * @return bool
     */
    public function view(User $user, Genre $genre): bool
    {
        return $this->can($user, $genre, 'view');
    }

    /**
     * Determine whether the User can create models.
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->can($user, new Genre(), 'create');
    }

    /**
     * Determine whether the User can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Genre $genre
     *
     * @return bool
     */
    public function update(User $user, Genre $genre): bool
    {
        return $this->can($user, $genre, 'update');
    }

    /**
     * Determine whether the User can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Genre $genre
     *
     * @return bool
     */
    public function delete(User $user, Genre $genre): bool
    {
        return $this->can($user, $genre, 'delete');
    }

    /**
     * Determine whether the User can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Genre $genre
     *
     * @return bool
     */
    public function restore(User $user, Genre $genre): bool
    {
        return $this->can($user, $genre, 'restore');
    }

    /**
     * Determine whether the User can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Genre $genre
     *
     * @return bool
     */
    public function forceDelete(User $user, Genre $genre): bool
    {
        return $this->can($user, $genre, 'forceDelete');
    }
}

<?php

namespace App\QueryBuilders;

use App\Http\Requests\GroupGetRequest;
use App\Models\Group;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

final class GroupBuilder extends Builder
{
    /**
     * Current HTTP Request object.
     *
     * @var GroupGetRequest
     */
    protected $request;

    /**
     * GroupBuilder constructor.
     *
     * @param GroupGetRequest $request
     */
    public function __construct(GroupGetRequest $request)
    {
        $this->request = $request;
        $this->builder = QueryBuilder::for(Group::class, $request);
    }

    /**
     * Get a list of allowed columns that can be selected.
     *
     * @return string[]
     */
    protected function getAllowedFields(): array
    {
        return [
            'groups.id',
            'groups.roles',
            'groups.created_at',
            'groups.updated_at',
        ];
    }

    /**
     * Get a list of allowed columns that can be used in any filter operations.
     *
     * @return array
     */
    protected function getAllowedFilters(): array
    {
        return [
            AllowedFilter::exact('id'),
            'roles',
            AllowedFilter::exact('created_at'),
            AllowedFilter::exact('updated_at'),
            AllowedFilter::exact('groups.id'),
            'groups.roles',
            AllowedFilter::exact('groups.created_at'),
            AllowedFilter::exact('groups.updated_at'),
        ];
    }

    /**
     * Get a list of allowed relationships that can be used in any include operations.
     *
     * @return string[]
     */
    protected function getAllowedIncludes(): array
    {
        return [
            'members',
        ];
    }

    /**
     * Get a list of allowed searchable columns which can be used in any search operations.
     *
     * @return string[]
     */
    protected function getAllowedSearch(): array
    {
        return [
            'roles',
        ];
    }

    /**
     * Get a list of allowed columns that can be used in any sort operations.
     *
     * @return string[]
     */
    protected function getAllowedSorts(): array
    {
        return [
            'id',
            'roles',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get the default sort column that will be used in any sort operation.
     *
     * @return string
     */
    protected function getDefaultSort(): string
    {
        return 'id';
    }
}

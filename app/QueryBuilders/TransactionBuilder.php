<?php

namespace App\QueryBuilders;

use App\Http\Requests\TransactionGetRequest;
use App\Models\Transaction;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

final class TransactionBuilder extends Builder
{
    /**
     * Current HTTP Request object.
     *
     * @var TransactionGetRequest
     */
    protected $request;

    /**
     * TransactionBuilder constructor.
     *
     * @param TransactionGetRequest $request
     */
    public function __construct(TransactionGetRequest $request)
    {
        $this->request = $request;
        $this->builder = QueryBuilder::for(Transaction::class, $request);
    }

    /**
     * Get a list of allowed columns that can be selected.
     *
     * @return string[]
     */
    protected function getAllowedFields(): array
    {
        return [
            'transactions.id',
            'transactions.book_id',
            'transactions.user_id',
            'transactions.date',
            'transactions.quantity',
            'transactions.total',
            'transactions.created_at',
            'transactions.updated_at',
            'book.id',
            'book.author_id',
            'book.title',
            'book.price',
            'book.created_at',
            'book.updated_at',
            'user.id',
            'user.name',
            'user.email',
            'user.email_verified_at',
            'user.password',
            'user.remember_token',
            'user.created_at',
            'user.updated_at',
        ];
    }

    /**
     * Get a list of allowed columns that can be used in any filter operations.
     *
     * @return array
     */
    protected function getAllowedFilters(): array
    {
        return [
            AllowedFilter::exact('id'),
            AllowedFilter::exact('book_id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('date'),
            AllowedFilter::exact('quantity'),
            AllowedFilter::exact('total'),
            AllowedFilter::exact('created_at'),
            AllowedFilter::exact('updated_at'),
            AllowedFilter::exact('transactions.id'),
            AllowedFilter::exact('transactions.book_id'),
            AllowedFilter::exact('transactions.user_id'),
            AllowedFilter::exact('transactions.date'),
            AllowedFilter::exact('transactions.quantity'),
            AllowedFilter::exact('transactions.total'),
            AllowedFilter::exact('transactions.created_at'),
            AllowedFilter::exact('transactions.updated_at'),
            AllowedFilter::exact('book.id'),
            AllowedFilter::exact('book.author_id'),
            'book.title',
            AllowedFilter::exact('book.price'),
            AllowedFilter::exact('book.created_at'),
            AllowedFilter::exact('book.updated_at'),
            AllowedFilter::exact('user.id'),
            'user.name',
            'user.email',
            AllowedFilter::exact('user.email_verified_at'),
            'user.password',
            'user.remember_token',
            AllowedFilter::exact('user.created_at'),
            AllowedFilter::exact('user.updated_at'),
        ];
    }

    /**
     * Get a list of allowed relationships that can be used in any include operations.
     *
     * @return string[]
     */
    protected function getAllowedIncludes(): array
    {
        return [
            'book',
            'user',

        ];
    }

    /**
     * Get a list of allowed searchable columns which can be used in any search operations.
     *
     * @return string[]
     */
    protected function getAllowedSearch(): array
    {
        return [
            'book.title',
            'user.name',
            'user.email',
            'user.password',
            'user.remember_token',
        ];
    }

    /**
     * Get a list of allowed columns that can be used in any sort operations.
     *
     * @return string[]
     */
    protected function getAllowedSorts(): array
    {
        return [
            'id',
            'book_id',
            'user_id',
            'date',
            'quantity',
            'total',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get the default sort column that will be used in any sort operation.
     *
     * @return string
     */
    protected function getDefaultSort(): string
    {
        return 'id';
    }
}

<?php

namespace App\QueryBuilders;

use App\Http\Requests\MemberGetRequest;
use App\Models\Member;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

final class MemberBuilder extends Builder
{
    /**
     * Current HTTP Request object.
     *
     * @var MemberGetRequest
     */
    protected $request;

    /**
     * MemberBuilder constructor.
     *
     * @param MemberGetRequest $request
     */
    public function __construct(MemberGetRequest $request)
    {
        $this->request = $request;
        $this->builder = QueryBuilder::for(Member::class, $request);
    }

    /**
     * Get a list of allowed columns that can be selected.
     *
     * @return string[]
     */
    protected function getAllowedFields(): array
    {
        return [
            'members.id',
            'members.name',
            'members.group_id',
            'members.created_at',
            'members.updated_at',
            'group.id',
            'group.roles',
            'group.created_at',
            'group.updated_at',
        ];
    }

    /**
     * Get a list of allowed columns that can be used in any filter operations.
     *
     * @return array
     */
    protected function getAllowedFilters(): array
    {
        return [
            AllowedFilter::exact('id'),
            'name',
            AllowedFilter::exact('group_id'),
            AllowedFilter::exact('created_at'),
            AllowedFilter::exact('updated_at'),
            AllowedFilter::exact('members.id'),
            'members.name',
            AllowedFilter::exact('members.group_id'),
            AllowedFilter::exact('members.created_at'),
            AllowedFilter::exact('members.updated_at'),
            AllowedFilter::exact('group.id'),
            'group.roles',
            AllowedFilter::exact('group.created_at'),
            AllowedFilter::exact('group.updated_at'),
        ];
    }

    /**
     * Get a list of allowed relationships that can be used in any include operations.
     *
     * @return string[]
     */
    protected function getAllowedIncludes(): array
    {
        return [
            'group',
        ];
    }

    /**
     * Get a list of allowed searchable columns which can be used in any search operations.
     *
     * @return string[]
     */
    protected function getAllowedSearch(): array
    {
        return [
            'name',
            'group.roles',
        ];
    }

    /**
     * Get a list of allowed columns that can be used in any sort operations.
     *
     * @return string[]
     */
    protected function getAllowedSorts(): array
    {
        return [
            'id',
            'name',
            'group_id',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get the default sort column that will be used in any sort operation.
     *
     * @return string
     */
    protected function getDefaultSort(): string
    {
        return 'id';
    }
}

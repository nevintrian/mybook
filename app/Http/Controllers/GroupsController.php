<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupSaveRequest;
use App\Http\Resources\GroupCollection;
use App\Http\Resources\GroupResource;
use App\Models\Group;
use App\QueryBuilders\GroupBuilder;
use Illuminate\Http\JsonResponse;

/**
 * @group Group Management
 *
 * API Endpoints for managing groups.
 */
class GroupsController extends Controller
{
    /**
     * Determine if any access to this resource require authorization.
     *
     * @var bool
     */
    protected static $requireAuthorization = true;

    /**
     * GroupsController constructor.
     */
    public function __construct()
    {
        if (self::$requireAuthorization || (auth()->user() !== null)) {
            $this->authorizeResource(Group::class);
        }
    }

    /**
     * Resource Collection.
     * Display a collection of the group resources in paginated document format.
     *
     * @authenticated
     *
     * @queryParam fields[groups] *string* - No-example
     * Comma-separated field/attribute names of the group resource to include in the response document.
     * The available fields for current endpoint are: `id`, `roles`, `created_at`, `updated_at`.
     * @queryParam page[size] *integer* - No-example
     * Describe how many records to display in a collection.
     * @queryParam page[number] *integer* - No-example
     * Describe the number of current page to display.
     * @queryParam include *string* - No-example
     * Comma-separated relationship names to include in the response document.
     * The available relationships for current endpoint are: `members`.
     * @queryParam sort *string* - No-example
     * Field/attribute to sort the resources in response document by.
     * The available fields for sorting operation in current endpoint are: `id`, `roles`, `created_at`, `updated_at`.
     * @queryParam filter[`filterName`] *string* - No-example
     * Filter the resources by specifying *attribute name* or *query scope name*.
     * The available filters for current endpoint are: `id`, `roles`, `created_at`, `updated_at`.
     * @qeuryParam search *string* - No-example
     * Filter the resources by specifying any keyword to search.
     *
     * @param \App\QueryBuilders\GroupBuilder $query
     *
     * @return GroupCollection
     */
    public function index(GroupBuilder $query): GroupCollection
    {
        return new GroupCollection($query->paginate());
    }

    /**
     * Create Resource.
     * Create a new group resource.
     *
     * @authenticated
     *
     * @param \App\Http\Requests\GroupSaveRequest $request
     * @param \App\Models\Group $group
     *
     * @return JsonResponse
     */
    public function store(GroupSaveRequest $request, Group $group): JsonResponse
    {
        $group->fill($request->only($group->offsetGet('fillable')))
            ->save();

        $resource = (new GroupResource($group))
            ->additional(['info' => 'The new group has been saved.']);

        return $resource->toResponse($request)->setStatusCode(201);
    }

    /**
     * Show Resource.
     * Display a specific group resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam group required *integer* - No-example
     * The identifier of a specific group resource.
     *
     * @queryParam fields[groups] *string* - No-example
     * Comma-separated field/attribute names of the group resource to include in the response document.
     * The available fields for current endpoint are: `id`, `roles`, `created_at`, `updated_at`.
     * @queryParam include *string* - No-example
     * Comma-separated relationship names to include in the response document.
     * The available relationships for current endpoint are: `members`.
     *
     * @param \App\QueryBuilders\GroupBuilder $query
     * @param \App\Models\Group $group
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *
     * @return GroupResource
     */
    public function show(GroupBuilder $query, Group $group): GroupResource
    {
        return new GroupResource($query->find($group->getKey()));
    }

    /**
     * Update Resource.
     * Update a specific group resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam group required *integer* - No-example
     * The identifier of a specific group resource.
     *
     * @param \App\Http\Requests\GroupSaveRequest $request
     * @param \App\Models\Group $group
     *
     * @return GroupResource
     */
    public function update(GroupSaveRequest $request, Group $group): GroupResource
    {
        $group->fill($request->only($group->offsetGet('fillable')));

        if ($group->isDirty()) {
            $group->save();
        }

        return (new GroupResource($group))
            ->additional(['info' => 'The group has been updated.']);
    }

    /**
     * Delete Resource.
     * Delete a specific group resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam group required *integer* - No-example
     * The identifier of a specific group resource.
     *
     * @param \App\Models\Group $group
     *
     * @throws \Exception
     *
     * @return GroupResource
     */
    public function destroy(Group $group): GroupResource
    {
        $group->delete();

        return (new GroupResource($group))
            ->additional(['info' => 'The group has been deleted.']);
    }
}

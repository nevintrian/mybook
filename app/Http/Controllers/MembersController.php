<?php

namespace App\Http\Controllers;

use App\Http\Requests\MemberSaveRequest;
use App\Http\Resources\MemberCollection;
use App\Http\Resources\MemberResource;
use App\Models\Member;
use App\QueryBuilders\MemberBuilder;
use Illuminate\Http\JsonResponse;

/**
 * @group Member Management
 *
 * API Endpoints for managing members.
 */
class MembersController extends Controller
{
    /**
     * Determine if any access to this resource require authorization.
     *
     * @var bool
     */
    protected static $requireAuthorization = true;

    /**
     * MembersController constructor.
     */
    public function __construct()
    {
        if (self::$requireAuthorization || (auth()->user() !== null)) {
            $this->authorizeResource(Member::class);
        }
    }

    /**
     * Resource Collection.
     * Display a collection of the member resources in paginated document format.
     *
     * @authenticated
     *
     * @queryParam fields[members] *string* - No-example
     * Comma-separated field/attribute names of the member resource to include in the response document.
     * The available fields for current endpoint are: `id`, `name`, `group_id`, `created_at`, `updated_at`.
     * @queryParam fields[group] *string* - No-example
     * Comma-separated field/attribute names of the group resource to include in the response document.
     * The available fields for current endpoint are: `id`, `roles`, `created_at`, `updated_at`.
     * @queryParam page[size] *integer* - No-example
     * Describe how many records to display in a collection.
     * @queryParam page[number] *integer* - No-example
     * Describe the number of current page to display.
     * @queryParam include *string* - No-example
     * Comma-separated relationship names to include in the response document.
     * The available relationships for current endpoint are: `group`.
     * @queryParam sort *string* - No-example
     * Field/attribute to sort the resources in response document by.
     * The available fields for sorting operation in current endpoint are: `id`, `name`, `group_id`, `created_at`, `updated_at`.
     * @queryParam filter[`filterName`] *string* - No-example
     * Filter the resources by specifying *attribute name* or *query scope name*.
     * The available filters for current endpoint are: `id`, `name`, `group_id`, `created_at`, `updated_at`, `group.id`, `group.roles`, `group.created_at`, `group.updated_at`.
     * @qeuryParam search *string* - No-example
     * Filter the resources by specifying any keyword to search.
     *
     * @param \App\QueryBuilders\MemberBuilder $query
     *
     * @return MemberCollection
     */
    public function index(MemberBuilder $query): MemberCollection
    {
        return new MemberCollection($query->paginate());
    }

    /**
     * Create Resource.
     * Create a new member resource.
     *
     * @authenticated
     *
     * @param \App\Http\Requests\MemberSaveRequest $request
     * @param \App\Models\Member $member
     *
     * @return JsonResponse
     */
    public function store(MemberSaveRequest $request, Member $member): JsonResponse
    {
        $member->fill($request->only($member->offsetGet('fillable')))
            ->save();

        $resource = (new MemberResource($member))
            ->additional(['info' => 'The new member has been saved.']);

        return $resource->toResponse($request)->setStatusCode(201);
    }

    /**
     * Show Resource.
     * Display a specific member resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam member required *integer* - No-example
     * The identifier of a specific member resource.
     *
     * @queryParam fields[members] *string* - No-example
     * Comma-separated field/attribute names of the member resource to include in the response document.
     * The available fields for current endpoint are: `id`, `name`, `group_id`, `created_at`, `updated_at`.
     * @queryParam fields[group] *string* - No-example
     * Comma-separated field/attribute names of the group resource to include in the response document.
     * The available fields for current endpoint are: `id`, `roles`, `created_at`, `updated_at`.
     * @queryParam include *string* - No-example
     * Comma-separated relationship names to include in the response document.
     * The available relationships for current endpoint are: `group`.
     *
     * @param \App\QueryBuilders\MemberBuilder $query
     * @param \App\Models\Member $member
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *
     * @return MemberResource
     */
    public function show(MemberBuilder $query, Member $member): MemberResource
    {
        return new MemberResource($query->find($member->getKey()));
    }

    /**
     * Update Resource.
     * Update a specific member resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam member required *integer* - No-example
     * The identifier of a specific member resource.
     *
     * @param \App\Http\Requests\MemberSaveRequest $request
     * @param \App\Models\Member $member
     *
     * @return MemberResource
     */
    public function update(MemberSaveRequest $request, Member $member): MemberResource
    {
        $member->fill($request->only($member->offsetGet('fillable')));

        if ($member->isDirty()) {
            $member->save();
        }

        return (new MemberResource($member))
            ->additional(['info' => 'The member has been updated.']);
    }

    /**
     * Delete Resource.
     * Delete a specific member resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam member required *integer* - No-example
     * The identifier of a specific member resource.
     *
     * @param \App\Models\Member $member
     *
     * @throws \Exception
     *
     * @return MemberResource
     */
    public function destroy(Member $member): MemberResource
    {
        $member->delete();

        return (new MemberResource($member))
            ->additional(['info' => 'The member has been deleted.']);
    }
}

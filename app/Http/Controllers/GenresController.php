<?php

namespace App\Http\Controllers;

use App\Http\Requests\GenreSaveRequest;
use App\Http\Resources\GenreCollection;
use App\Http\Resources\GenreResource;
use App\Models\Genre;
use App\QueryBuilders\GenreBuilder;
use Illuminate\Http\JsonResponse;

/**
 * @group Genre Management
 *
 * API Endpoints for managing genres.
 */
class GenresController extends Controller
{
    /**
     * Determine if any access to this resource require authorization.
     *
     * @var bool
     */
    protected static $requireAuthorization = false;

    /**
     * GenresController constructor.
     */
    public function __construct()
    {
        if (self::$requireAuthorization || (auth()->user() !== null)) {
            $this->authorizeResource(Genre::class);
        }
    }

    /**
     * Resource Collection.
     * Display a collection of the genre resources in paginated document format.
     *
     * @authenticated
     *
     * @queryParam fields[genres] *string* - No-example
     * Comma-separated field/attribute names of the genre resource to include in the response document.
     * The available fields for current endpoint are: `id`, `name`, `created_at`, `updated_at`.
     * @queryParam page[size] *integer* - No-example
     * Describe how many records to display in a collection.
     * @queryParam page[number] *integer* - No-example
     * Describe the number of current page to display.
     * @queryParam include *string* - No-example
     * Comma-separated relationship names to include in the response document.
     * The available relationships for current endpoint are: `books`.
     * @queryParam sort *string* - No-example
     * Field/attribute to sort the resources in response document by.
     * The available fields for sorting operation in current endpoint are: `id`, `name`, `created_at`, `updated_at`.
     * @queryParam filter[`filterName`] *string* - No-example
     * Filter the resources by specifying *attribute name* or *query scope name*.
     * The available filters for current endpoint are: `id`, `name`, `created_at`, `updated_at`.
     * @qeuryParam search *string* - No-example
     * Filter the resources by specifying any keyword to search.
     *
     * @param \App\QueryBuilders\GenreBuilder $query
     *
     * @return GenreCollection
     */
    public function index(GenreBuilder $query): GenreCollection
    {
        return new GenreCollection($query->paginate());
    }

    /**
     * Create Resource.
     * Create a new genre resource.
     *
     * @authenticated
     *
     * @param \App\Http\Requests\GenreSaveRequest $request
     * @param \App\Models\Genre $genre
     *
     * @return JsonResponse
     */
    public function store(GenreSaveRequest $request, Genre $genre): JsonResponse
    {
        $genre->fill($request->only($genre->offsetGet('fillable')))
            ->save();

        $resource = (new GenreResource($genre))
            ->additional(['info' => 'The new genre has been saved.']);

        return $resource->toResponse($request)->setStatusCode(201);
    }

    /**
     * Show Resource.
     * Display a specific genre resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam genre required *integer* - No-example
     * The identifier of a specific genre resource.
     *
     * @queryParam fields[genres] *string* - No-example
     * Comma-separated field/attribute names of the genre resource to include in the response document.
     * The available fields for current endpoint are: `id`, `name`, `created_at`, `updated_at`.
     * @queryParam include *string* - No-example
     * Comma-separated relationship names to include in the response document.
     * The available relationships for current endpoint are: `books`.
     *
     * @param \App\QueryBuilders\GenreBuilder $query
     * @param \App\Models\Genre $genre
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *
     * @return GenreResource
     */
    public function show(GenreBuilder $query, Genre $genre): GenreResource
    {
        return new GenreResource($query->find($genre->getKey()));
    }

    /**
     * Update Resource.
     * Update a specific genre resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam genre required *integer* - No-example
     * The identifier of a specific genre resource.
     *
     * @param \App\Http\Requests\GenreSaveRequest $request
     * @param \App\Models\Genre $genre
     *
     * @return GenreResource
     */
    public function update(GenreSaveRequest $request, Genre $genre): GenreResource
    {
        $genre->fill($request->only($genre->offsetGet('fillable')));

        if ($genre->isDirty()) {
            $genre->save();
        }

        return (new GenreResource($genre))
            ->additional(['info' => 'The genre has been updated.']);
    }

    /**
     * Delete Resource.
     * Delete a specific genre resource identified by the given id/key.
     *
     * @authenticated
     *
     * @urlParam genre required *integer* - No-example
     * The identifier of a specific genre resource.
     *
     * @param \App\Models\Genre $genre
     *
     * @throws \Exception
     *
     * @return GenreResource
     */
    public function destroy(Genre $genre): GenreResource
    {
        $genre->delete();

        return (new GenreResource($genre))
            ->additional(['info' => 'The genre has been deleted.']);
    }
}

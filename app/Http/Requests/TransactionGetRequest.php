<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionGetRequest extends FormRequest
{
    /**
     * Determine if the current user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
//        return (auth()->guard('api')->check() || auth()->guard('cms-api')->check());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'filter.id' => 'integer|between:0,18446744073709551615',
            'filter.book_id' => 'integer|between:0,18446744073709551615',
            'filter.user_id' => 'integer|between:0,18446744073709551615',
            'filter.date' => 'date',
            'filter.quantity' => 'integer|between:-2147483647,2147483647',
            'filter.total' => 'integer|between:-2147483647,2147483647',
            'filter.created_at' => 'date',
            'filter.updated_at' => 'date',
            'filter.transactions\.id' => 'integer|between:0,18446744073709551615',
            'filter.transactions\.book_id' => 'integer|between:0,18446744073709551615',
            'filter.transactions\.user_id' => 'integer|between:0,18446744073709551615',
            'filter.transactions\.date' => 'date',
            'filter.transactions\.quantity' => 'integer|between:-2147483647,2147483647',
            'filter.transactions\.total' => 'integer|between:-2147483647,2147483647',
            'filter.transactions\.created_at' => 'date',
            'filter.transactions\.updated_at' => 'date',
            'filter.book\.id' => 'integer|between:0,18446744073709551615',
            'filter.book\.author_id' => 'integer|between:0,18446744073709551615',
            'filter.book\.title' => 'string|min:2|max:255',
            'filter.book\.price' => 'integer|between:-2147483647,2147483647',
            'filter.book\.created_at' => 'date',
            'filter.book\.updated_at' => 'date',
            'filter.user\.id' => 'integer|between:0,18446744073709551615',
            'filter.user\.name' => 'string|min:2|max:255',
            'filter.user\.email' => 'string|email|min:11|max:255',
            'filter.user\.email_verified_at' => 'date',
            'filter.user\.password' => 'string|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/',
            'filter.user\.remember_token' => 'string|min:2|max:100',
            'filter.user\.created_at' => 'date',
            'filter.user\.updated_at' => 'date',
            'page.number' => 'integer|min:1',
            'page.size' => 'integer|between:1,100',
            'search' => 'nullable|string|min:3|max:60',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionSaveRequest extends FormRequest
{
    /**
     * Determine if the current user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
//        return (auth()->guard('api')->check() || auth()->guard('cms-api')->check());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'book_id' => 'required|integer|between:0,18446744073709551615',
            'user_id' => 'required|integer|between:0,18446744073709551615',
            'date' => 'required|date',
            'quantity' => 'required|integer|between:-2147483647,2147483647',
            'total' => 'required|integer|between:-2147483647,2147483647',
        ];
    }
}

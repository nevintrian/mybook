<?php

namespace App\Http\Livewire\Cms\Transactions;

use App\Models\Transaction;
use Cms\Livewire\Concerns\ResolveCurrentAdmin;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

abstract class TransactionForm extends Component
{
    use AuthorizesRequests;
    use ResolveCurrentAdmin;

    /**
     * The related transaction instance.
     *
     * @var Transaction
     */
    public Transaction $transaction;

    /**
     * Define the current operation of the livewire component.
     * The valid options for operation values are: create, view, update.
     *
     * @var string
     */
    protected string $operation;

    /**
     * The validation rules for transaction model.
     *
     * @var string[]
     */
    protected array $rules = [
        'transaction.book_id' => 'required|integer|between:0,18446744073709551615',
        'transaction.user_id' => 'required|integer|between:0,18446744073709551615',
        'transaction.date' => 'required|date',
        'transaction.quantity' => 'required|integer|between:-2147483647,2147483647',
        'transaction.total' => 'required|integer|between:-2147483647,2147483647',
    ];

    /**
     * Redirect and go back to index page.
     *
     * @return mixed
     */
    public function backToIndex()
    {
        return redirect()->to(
            route('cms.transactions.index')
        );
    }

    /**
     * Confirm Admin authorization to access the datatable resources.
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \ErrorException
     */
    protected function confirmAuthorization(): void
    {
        $permission = 'cms.' . $this->transaction->getTable() . '.' . $this->operation;

        if (!$this->getCurrentAdminProperty()->can($permission)) {
            throw new AuthorizationException();
        }
    }

    /**
     * Redirect to the edit transaction page.
     *
     * @return mixed
     */
    public function edit()
    {
        return redirect()->to(
            route('cms.transactions.edit', ['transaction' => $this->transaction])
        );
    }

    /**
     * Provide the breadcrumb items for the current livewire component.
     *
     * @return array[]
     */
    public function getBreadcrumbItemsProperty(): array
    {
        return [
            [
                'title' => 'Transactions',
                'url' => route('cms.transactions.index'),
            ]
        ];
    }

    /**
     * Get the success message after `save` action called successfully.
     *
     * @return string
     */
    protected function getSuccessMessage(): string
    {
        return ($this->operation === 'create') ?
            'The new transaction has been saved.' :
            'The transaction has been updated.';
    }

    /**
     * Handle the `mount` lifecycle event.
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \ErrorException
     */
    public function mount(): void
    {
        $this->confirmAuthorization();
    }

    /**
     * Save the transaction model.
     *
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \ErrorException
     */
    public function save()
    {
        if (($this->operation !== 'create') && ($this->operation !== 'update')) {
            return redirect()->to(route('cms.transactions.index'));
        }

        $this->confirmAuthorization();
        $this->validate();

        $this->transaction->save();

        session()->flash('alertType', 'success');
        session()->flash('alertMessage', $this->getSuccessMessage());

        return redirect()->to(route('cms.transactions.index'));
    }
}

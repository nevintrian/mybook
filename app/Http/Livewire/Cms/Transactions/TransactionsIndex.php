<?php

namespace App\Http\Livewire\Cms\Transactions;

use App\Models\Transaction;
use Cms\Livewire\DatatableColumn;
use Cms\Livewire\DatatableComponent;
use Illuminate\Database\Eloquent\Builder;

class TransactionsIndex extends DatatableComponent
{
    /**
     * Specify the datatable's columns and their behaviors.
     *
     * @return array
     */
    public function columns(): array
    {
        return $this->applyColumnVisibility([
            DatatableColumn::make('id'),
            // DatatableColumn::make('book_id'),
            // DatatableColumn::make('user_id'),
            DatatableColumn::make('date'),
            DatatableColumn::make('book.title'),

            DatatableColumn::make('quantity'),
            DatatableColumn::make('total'),
            // DatatableColumn::make('created_at'),
            // DatatableColumn::make('updated_at'),

            // DatatableColumn::make('book.id'),
            // DatatableColumn::make('book.author_id'),
            DatatableColumn::make('book.price'),
            // DatatableColumn::make('book.created_at'),
            // DatatableColumn::make('book.updated_at'),

            // DatatableColumn::make('user.id'),
            DatatableColumn::make('user.name'),
            DatatableColumn::make('user.email'),
            // DatatableColumn::make('user.email_verified_at'),
            // DatatableColumn::make('user.password'),
            // DatatableColumn::make('user.remember_token'),
            // DatatableColumn::make('user.created_at'),
            // DatatableColumn::make('user.updated_at'),
        ]);
    }

    /**
     * Defines the base route name for current datatable component.
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'cms.transactions.';
    }

    /**
     * Provide the breadcrumb items for the current livewire component.
     *
     * @return array[]
     */
    public function getBreadcrumbItemsProperty(): array
    {
        return [
            [
                'title' => 'Transactions',
                'url' => route($this->getBaseRouteName() . 'index'),
            ]
        ];
    }

    /**
     * Get a new query builder instance for the current datatable component.
     * You may include the model's relationships if it's necessary.
     *
     * @return Builder
     */
    protected function newQuery(): Builder
    {
        return (new Transaction())
            ->newQuery()
            ->with(['book', 'user']);
    }

    /**
     * Render the LiveWire component.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|mixed
     */
    public function render()
    {
        return view('livewire.cms.transactions.transactions_index')
            ->extends('cms::_layouts.app')
            ->section('content');
    }

    /**
     * Specify the searchable column names in the current datatable component.
     *
     * @return array
     */
    protected function searchableColumns(): array
    {
        return [
            'book.title',
            'user.name',
            'user.email',
            'user.password',
            'user.remember_token',
        ];
    }
}

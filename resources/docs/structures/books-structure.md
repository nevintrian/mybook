## Books Resource Structure

| Column | Data Type | Description |
| ------ | --------- | ----------- |
| id | bigint, unsigned |  |
| author_id | bigint, unsigned |  |
| title | string (255) |  |
| price | integer |  |
| created_at | datetime, nullable |  |
| updated_at | datetime, nullable |  |


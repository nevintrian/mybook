## Groups Resource Structure

| Column | Data Type | Description |
| ------ | --------- | ----------- |
| id | bigint, unsigned |  |
| roles | string (255) |  |
| created_at | datetime, nullable |  |
| updated_at | datetime, nullable |  |


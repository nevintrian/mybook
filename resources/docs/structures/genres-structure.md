## Genres Resource Structure

| Column | Data Type | Description |
| ------ | --------- | ----------- |
| id | bigint, unsigned |  |
| name | string (255) |  |
| created_at | datetime, nullable |  |
| updated_at | datetime, nullable |  |


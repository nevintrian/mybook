## Members Resource Structure

| Column | Data Type | Description |
| ------ | --------- | ----------- |
| id | bigint, unsigned |  |
| name | string (255) |  |
| group_id | bigint, unsigned |  |
| created_at | datetime, nullable |  |
| updated_at | datetime, nullable |  |


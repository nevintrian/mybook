## Transactions Resource Structure

| Column | Data Type | Description |
| ------ | --------- | ----------- |
| id | bigint, unsigned |  |
| book_id | bigint, unsigned |  |
| user_id | bigint, unsigned |  |
| date | date |  |
| quantity | integer |  |
| total | integer |  |
| created_at | datetime, nullable |  |
| updated_at | datetime, nullable |  |


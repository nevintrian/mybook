Hi Nabh dan Nevin,

Penerbit buku ingin menjual buku-buku mereka secara online.

Buku yang mereka miliki:

-   Salvation of a Saint
    -   Harga: 79.200
    -   Genre: mystery, romance, crime
    -   Author: Keigo Higashino
-   Pembunuhan di Nihonbashi
    -   Harga: 93.000
    -   Genre: mystery, murder, crime
    -   Author: Keigo Higashino
-   Haru Mahameru
    -   Harga: 69.000
    -   Genre: romance, fantasy, adventure
    -   Author: Balakarsa
-   Trouvaille (New Edition)
    -   Harga: 99.500
    -   Genre: romance, comedy
    -   Author: Febie Gusfa
-   Midwinter Murder
    -   Harga: 179.000
    -   Genre: mystery, crime, murder
    -   Author: Agatha Christie
-   The Labors of Hercules
    -   Harga: 149.000
    -   Genre: romance, crime, mystery
    -   Author: Agatha Christie
-   The Lie Tree
    -   Harga: 115.000
    -   Genre: fantasy, mystery, historical
    -   Author: Frances Hardinge

Tolong dibuatkan:

Menggunakan CMS generator:

-   CMS CRUD Genre.
-   CMS CRUD Book.
-   CMS CRUD Author.
-   CMS Transaction (List + Detail).

Menggunakan API generator:

-   API endpoint untuk menampilkan list masing2 Model.
-   API endpoint untuk menampilkan Book based on Genre.
-   API endpoint untuk menampilkan Book based on Author.
    (hint: manfaatkan spatie query builder)
-   API User login (Model User) (hint: gunakan sanctum + manfaatkan token).
    -   User bisa beli buku (API endpoint) dengan flow:
        -   Pilih buku + jumlah (transaksi per buku saja, tidak perlu sampai ke beberapa buku di 1 transaksi yang sama).
        -   User + Admin dapat email notifikasi informasi pembelian.
-   tambahkan automatic backup database saja ke dropbox 1 kali sehari.

Untuk API, jadi nanti semua kegiatan dilakukan di Postman/Insomnia (tidak perlu UI untuk public view).

<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Home\Index')->name('home');
Route::get('/current-admin/profile', 'CurrentAdmin\Profile')->name('current-admin.profile');

/**
 * Begin route definition for `Settings` resources.
 */
Route::get('/settings', 'Settings\SettingsIndex')->name('settings.index');
Route::get('/settings/create', 'Settings\CreateSetting')->name('settings.create');
Route::get('/settings/{setting}', 'Settings\ShowSetting')->name('settings.show');
Route::get('/settings/{setting}/edit', 'Settings\EditSetting')->name('settings.edit');

/**
 * Begin route definition for `Roles` resources.
 */
Route::get('/roles', 'Roles\RolesIndex')->name('roles.index');
Route::get('/roles/create', 'Roles\CreateRole')->name('roles.create');
Route::get('/roles/{role}', 'Roles\ShowRole')->name('roles.show');
Route::get('/roles/{role}/edit', 'Roles\EditRole')->name('roles.edit');

/**
 * Begin route definition for `Admins` resources.
 */
Route::get('/admins', 'Admins\AdminsIndex')->name('admins.index');
Route::get('/admins/create', 'Admins\CreateAdmin')->name('admins.create');
Route::get('/admins/{admin}', 'Admins\ShowAdmin')->name('admins.show');
Route::get('/admins/{admin}/edit', 'Admins\EditAdmin')->name('admins.edit');

/**
 * Begin route definition for `Seo Metas` resources.
 */
Route::get('/seo_metas', 'SeoMetas\SeoMetasIndex')->name('seo_metas.index');
Route::get('/seo_metas/create', 'SeoMetas\CreateSeoMeta')->name('seo_metas.create');
Route::get('/seo_metas/{seoMeta}', 'SeoMetas\ShowSeoMeta')->name('seo_metas.show');
Route::get('/seo_metas/{seoMeta}/edit', 'SeoMetas\EditSeoMeta')->name('seo_metas.edit');

/**
 * Begin route definition for `Static Pages` resources.
 */
Route::get('/static_pages', 'StaticPages\StaticPagesIndex')->name('static_pages.index');
Route::get('/static_pages/create', 'StaticPages\CreateStaticPage')->name('static_pages.create');
Route::get('/static_pages/{staticPage}', 'StaticPages\ShowStaticPage')->name('static_pages.show');
Route::get('/static_pages/{staticPage}/edit', 'StaticPages\EditStaticPage')->name('static_pages.edit');


/**
 * Begin route definition for `Books` resources.
 */
Route::get('/books', 'Books\BooksIndex')->name('books.index');
Route::get('/books/create', 'Books\CreateBook')->name('books.create');
Route::get('/books/{book}', 'Books\ShowBook')->name('books.show');
Route::get('/books/{book}/edit', 'Books\EditBook')->name('books.edit');


/**
 * Begin route definition for `Authors` resources.
 */
Route::get('/authors', 'Authors\AuthorsIndex')->name('authors.index');
Route::get('/authors/create', 'Authors\CreateAuthor')->name('authors.create');
Route::get('/authors/{author}', 'Authors\ShowAuthor')->name('authors.show');
Route::get('/authors/{author}/edit', 'Authors\EditAuthor')->name('authors.edit');


/**
 * Begin route definition for `Genres` resources.
 */
Route::get('/genres', 'Genres\GenresIndex')->name('genres.index');
Route::get('/genres/create', 'Genres\CreateGenre')->name('genres.create');
Route::get('/genres/{genre}', 'Genres\ShowGenre')->name('genres.show');
Route::get('/genres/{genre}/edit', 'Genres\EditGenre')->name('genres.edit');


/**
 * Begin route definition for `Transactions` resources.
 */
Route::get('/transactions', 'Transactions\TransactionsIndex')->name('transactions.index');
Route::get('/transactions/create', 'Transactions\CreateTransaction')->name('transactions.create');
Route::get('/transactions/{transaction}', 'Transactions\ShowTransaction')->name('transactions.show');
Route::get('/transactions/{transaction}/edit', 'Transactions\EditTransaction')->name('transactions.edit');


/**
 * Begin route definition for `Members` resources.
 */
Route::get('/members', 'Members\MembersIndex')->name('members.index');
Route::get('/members/create', 'Members\CreateMember')->name('members.create');
Route::get('/members/{member}', 'Members\ShowMember')->name('members.show');
Route::get('/members/{member}/edit', 'Members\EditMember')->name('members.edit');


/**
 * Begin route definition for `Groups` resources.
 */
Route::get('/groups', 'Groups\GroupsIndex')->name('groups.index');
Route::get('/groups/create', 'Groups\CreateGroup')->name('groups.create');
Route::get('/groups/{group}', 'Groups\ShowGroup')->name('groups.show');
Route::get('/groups/{group}/edit', 'Groups\EditGroup')->name('groups.edit');

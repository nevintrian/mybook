<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(SeoMetaSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(StaticPageSeeder::class);
        $this->call(AuthorSeeder::class);
        $this->call(GenreSeeder::class);
        $this->call(BookSeeder::class);
        $this->call(BookGenreSeeder::class);
        $this->call(TransactionSeeder::class);
        $this->call(MemberSeeder::class);
        $this->call(MemberSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(GroupSeeder::class);
    }
}

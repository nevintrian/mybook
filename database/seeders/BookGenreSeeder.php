<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\BookGenre;
use App\Models\Genre;
use Illuminate\Database\Seeder;

class BookGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookIds = Book::pluck('id')->toArray();
        $genreIds = Genre::pluck('id')->toArray();

        foreach ($bookIds as $bookId) {
            foreach ($genreIds as $genreId) {
                BookGenre::factory(5)->create([
                    'book_id' => $bookId,
                    'genre_id' => $genreId,
                ]);
            }
        }
    }
}

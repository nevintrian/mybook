<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookIds = Book::pluck('id')->toArray();
        $userIds = User::pluck('id')->toArray();

        foreach ($bookIds as $bookId) {
            foreach ($userIds as $userId) {
                Transaction::factory(5)->create([
                    'book_id' => $bookId,
                    'user_id' => $userId,
                ]);
            }
        }
    }
}

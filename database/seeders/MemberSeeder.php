<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Member;
use Illuminate\Database\Seeder;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupIds = Group::pluck('id')->toArray();

        foreach ($groupIds as $groupId) {
            Member::factory(5)->create([
                'group_id' => $groupId,
            ]);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authorIds = Author::pluck('id')->toArray();

        foreach ($authorIds as $authorId) {
            Book::factory(5)->create([
                'author_id' => $authorId,
            ]);
        }
    }
}

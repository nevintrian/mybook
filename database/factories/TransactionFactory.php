<?php

namespace Database\Factories;

use App\Models\Book;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Carbon\Carbon;

class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'book_id' => Book::factory(),
            'user_id' => User::factory(),
            'date' => Carbon::today()->subDays(rand(1, 60)),
            'quantity' => $this->faker->numberBetween(0, 99999),
            'total' => $this->faker->numberBetween(0, 99999),

        ];
    }
}
